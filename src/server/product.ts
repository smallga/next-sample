import { Product } from '@/interface/product.interface'
import axios from 'axios'

const urlProfix = process.env.API_HOSTNAME

export async function getProductById(id: string | number): Promise<Product> {
  const { data: response } = await axios(`${urlProfix}/api/product/${id}`)
  return response
}

export async function getProducts(): Promise<Product[]> {
  const { data: response } = await axios(`${urlProfix}/api/products`)
  return response
}
