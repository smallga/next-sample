interface LoadingProps {}
export default function Loading(props: LoadingProps) {
  return (
    <div className="lds-ring">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}
