import Loading from '@/components/loading'
import useGetProductById from '@/hook/useGetProductByID'
import { getProductById } from '@/server/product'
import { dehydrate, DehydratedState, QueryClient } from '@tanstack/react-query'
import {
  GetStaticProps,
  GetStaticPropsContext,
  GetStaticPropsResult,
} from 'next'
import { useRouter } from 'next/router'
import { ParsedUrlQuery } from 'querystring'

interface ProductProps {}

interface Params extends ParsedUrlQuery {
  id: string
}

type Props = {
  dehydratedState: DehydratedState
}

export default function Product(props: ProductProps) {
  const router = useRouter()
  const { id } = router.query
  const { isError, isLoading, data } = useGetProductById(id as string)

  return <div>{isLoading ? <Loading /> : <div>{data?.productName}</div>}</div>
}

export async function getStaticPaths() {
  return {
    paths: [{ params: { id: '1' } }, { params: { id: '2' } }],
    fallback: 'blocking',
  }
}

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  const id = context.params?.id
  const queryClient = new QueryClient()

  await queryClient.fetchQuery(['product', id], () =>
    getProductById(id as string)
  )

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
      revalidate: 3153600,
    },
  }
}
