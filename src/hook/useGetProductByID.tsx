import { getProductById } from '@/server/product'
import { useQuery } from '@tanstack/react-query'

const useGetProductById = (id: string | number) => {
  return useQuery({
    queryKey: ['product', id],
    queryFn: () => getProductById(id),
  })
}

export default useGetProductById
